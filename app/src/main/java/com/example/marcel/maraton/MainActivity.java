package com.example.marcel.maraton;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeCadencePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusBikeSpdCadCommonPcc;
import com.dsi.ant.plugins.antplus.pccbase.AsyncScanController;
import com.dsi.ant.plugins.antplus.pccbase.PccReleaseHandle;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.EnumSet;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    AsyncScanController.AsyncScanResultDeviceInfo srdce = null;
    TextView hrText;
    AntPlusHeartRatePcc hrPcc = null;
    protected PccReleaseHandle<AntPlusHeartRatePcc> hrReleaseHandle = null;
    AsyncScanController<AntPlusHeartRatePcc> hrScanCtrl;

    AsyncScanController.AsyncScanResultDeviceInfo moxy = null;
    AsyncScanController.AsyncScanResultDeviceInfo moxy2 = null;
    TextView moxyText;
    AntPlusBikeCadencePcc moxyPcc = null;
    protected PccReleaseHandle<AntPlusBikeCadencePcc> moxyReleaseHandle = null;
    protected PccReleaseHandle<AntPlusBikeCadencePcc> moxy2ReleaseHandle = null;
    AsyncScanController<AntPlusBikeCadencePcc> moxyScanCtrl;
    AsyncScanController<AntPlusBikeCadencePcc> moxy2ScanCtrl;

    TextView locationText;
    TextView netText;
    TextView systemText;

    private static final String[] PERMS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.INTERNET,
            android.Manifest.permission.ACCESS_NETWORK_STATE
    };

    String lastHeartRate;
    String lastMoxy;
    String lastFlow;
    String lastMoxy2;
    String lastFlow2;
    String lastLocationLat;
    String lastLocationLon;
    String lastSpeed;
    String lastLocationAccuracy;
    String lastPressure;

    Button start1;
    Button start2;

    String srdceStart;
    String moxyStart;

    String personNumber;

    boolean shortStart = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
//            @Override
//            public void uncaughtException(Thread thread, Throwable ex) {
//                systemText.setText(ex.getMessage());
//            }
//        });
//        if(!hasPermission(android.Manifest.permission.INTERNET)) {
//            requestPermissions(PERMS, 1338);
//        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hrText = (TextView) findViewById(R.id.hrText);
        moxyText = (TextView) findViewById(R.id.moxyText);
        locationText = (TextView) findViewById(R.id.locationText);
        netText = (TextView) findViewById(R.id.netText);
        systemText = (TextView) findViewById(R.id.systemText);

//        start1 = (Button) findViewById(R.id.button);
//        start2 = (Button) findViewById(R.id.button2);
        startApp("1");

//        start1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startApp("1");
//            }
//        });
//        start2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startApp("2");
//            }
//        });
    }

    private void startApp(String numToUse) {
        personNumber = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//        disableButtons();
//        srdceStart = "srdce" + numToUse;
//        moxyStart = "moxy" + numToUse;
//        startHRSearch();
//        startMoxySearch();
        startGPS();
        startBarometer();
        startTimer();
    }

    private void disableButtons() {
        start1.setEnabled(false);
        start2.setEnabled(false);
    }

    private void startTimer() {
        Timer timer = new Timer("sender");
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Date date = new Date();
                String msg = "time=" + date.getTime();
                msg += "&person=" + personNumber;
                if(lastLocationLat != null) {
                    msg += "&lat=" + lastLocationLat + "&lon=" + lastLocationLon + "&acc=" + lastLocationAccuracy + "&spd=" + lastSpeed;
                    lastLocationLon = lastLocationLat = lastLocationAccuracy = lastSpeed = null;
                }

                if(lastMoxy != null) {
                    msg += "&mox=" + lastMoxy;
                    lastMoxy = null;
                }

                if(lastFlow != null) {
                    msg += "&flow=" + lastFlow;
                    lastFlow = null;
                }

                if(lastMoxy2 != null) {
                    msg += "&mox2=" + lastMoxy2;
                    lastMoxy2 = null;
                }

                if(lastFlow2 != null) {
                    msg += "&flow2=" + lastFlow2;
                    lastFlow2 = null;
                }

                if(lastHeartRate != null) {
                    msg += "&hr=" + lastHeartRate;
                    lastHeartRate = null;
                }

                if(lastPressure != null) {
                    msg += "&p=" + lastPressure;
                    lastPressure = null;
                }

                final String outMsg = msg;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        netText.setText(outMsg);
                    }
                });
                try {
                    downloadUrl("http://livestreamspartanrace.com/api.php?" + msg);
                } catch (IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            netText.setText("Error sending data");
                        }
                    });
                }
            }
        }, 5000, 5000);
    }

    private void startBarometer() {
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor pressure = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        if(pressure != null) {
            sensorManager.registerListener(new SensorEventListener() {
                @Override
                public void onSensorChanged(SensorEvent event) {
                    if(event.values.length > 0) {
                        lastPressure = Float.toString(event.values[0]);
                    }
                }

                @Override
                public void onAccuracyChanged(Sensor sensor, int accuracy) {

                }
            }, pressure, 2000);
        }
    }

    private void downloadUrl(String myurl) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        // Only display the first 500 characters of the retrieved
        // web page content.
        int len = 500;

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(4000 /* milliseconds */);
            conn.setConnectTimeout(4000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            is = conn.getInputStream();
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }


    public void startHRSearch()
    {
        hrText.setText("Searching");
        hrScanCtrl = AntPlusHeartRatePcc.requestAsyncScanController(this, 0, new AsyncScanController.IAsyncScanResultReceiver() {
            @Override
            public void onSearchStopped(RequestAccessResult requestAccessResult) {
                hrText.setText("Seach stopped");
                if(srdce == null) {
                    startHRSearch();
                }
            }

            @Override
            public void onSearchResult(AsyncScanController.AsyncScanResultDeviceInfo asyncScanResultDeviceInfo) {
                hrText.append("\n");
                String displayName = asyncScanResultDeviceInfo.getDeviceDisplayName();
                hrText.append(displayName);
                if(displayName.startsWith(srdceStart) && srdce == null) {
                    setSrdce(asyncScanResultDeviceInfo);
                }
            }
        });
    }

//    private boolean hasPermission(String perm) {

    public void startGPS()
    {
//        if(!hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
//            requestPermissions(PERMS, 1337);
//        }
        locationText.setText("Setting GPS");
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                final Location loc = location;
                lastLocationLat = Double.toString(location.getLatitude());
                lastLocationLon = Double.toString(location.getLongitude());
                lastSpeed = Float.toString(location.getSpeed());
                lastLocationAccuracy = Float.toString(location.getAccuracy());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        locationText.setText(loc.getLatitude() + " / " + loc.getLongitude() + " : " + loc.getAccuracy());
                    }
                });
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                locationText.setText("Status changed" + provider + ": " + status);
            }

            @Override
            public void onProviderEnabled(String provider) {
                locationText.setText("Provider enabled" + provider);
            }

            @Override
            public void onProviderDisabled(String provider) {
                locationText.setText("Provider disabled" + provider);
            }
        };
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, locationListener);
        } catch (SecurityException se) {
            final String message = se.getMessage();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    locationText.setText("Security exception: " + message);
                }
            });
        }
    }


    public void setSrdce(AsyncScanController.AsyncScanResultDeviceInfo data)
    {
        srdce = data;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hrText.append("\nFound srdce");
            }
        });
        hrReleaseHandle = hrScanCtrl.requestDeviceAccess(data, new AntPluginPcc.IPluginAccessResultReceiver<AntPlusHeartRatePcc>() {
            @Override
            public void onResultReceived(AntPlusHeartRatePcc antPlusHeartRatePcc, RequestAccessResult requestAccessResult, DeviceState deviceState) {
                antPlusHeartRatePcc.subscribeHeartRateDataEvent(new AntPlusHeartRatePcc.IHeartRateDataReceiver()
                {
                    @Override
                    public void onNewHeartRateData(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                                                   final int computedHeartRate, final long heartBeatCount,
                                                   final BigDecimal heartBeatEventTime, final AntPlusHeartRatePcc.DataState dataState)
                    {
                        if(dataState == AntPlusHeartRatePcc.DataState.LIVE_DATA) {
                            lastHeartRate = Integer.toString(computedHeartRate);
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hrText.setText("HR:" + computedHeartRate + " / " + dataState + " / " + estTimestamp);
                            }
                        });
                    }
                });
            }
        }, base_IDeviceStateChangeReceiver);
    }

    protected AntPluginPcc.IDeviceStateChangeReceiver base_IDeviceStateChangeReceiver =
            new AntPluginPcc.IDeviceStateChangeReceiver()
            {
                @Override
                public void onDeviceStateChange(final DeviceState newDeviceState)
                {
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            hrText.append("\nNew state: " + newDeviceState);
                        }
                    });
                }
            };

    //    }
//        return(PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    public void startMoxySearch()
    {
        moxyText.setText("Searching");

        moxyScanCtrl = AntPlusBikeCadencePcc.requestAsyncScanController(this, 0, new AntPlusBikeSpdCadCommonPcc.IBikeSpdCadAsyncScanResultReceiver() {
            AntPlusBikeSpdCadCommonPcc.BikeSpdCadAsyncScanResultDeviceInfo moxy1Info = null;
            AntPlusBikeSpdCadCommonPcc.BikeSpdCadAsyncScanResultDeviceInfo moxy2Info = null;
            @Override
            public void onSearchStopped(RequestAccessResult requestAccessResult) {
                if(moxy == null) {
                    startMoxySearch();
                }
            }
            @Override
            public void onSearchResult(AntPlusBikeSpdCadCommonPcc.BikeSpdCadAsyncScanResultDeviceInfo bikeSpdCadAsyncScanResultDeviceInfo) {
                String name = bikeSpdCadAsyncScanResultDeviceInfo.resultInfo.getDeviceDisplayName();
                if(name.startsWith(moxyStart)) {
                    if(moxy == null) {
                        setMoxy(bikeSpdCadAsyncScanResultDeviceInfo.resultInfo);
                        startMoxy2Search();
                    }
                }
            }
        });
    }

    public void startMoxy2Search()
    {
        moxyText.append("\nSearching2");

        moxy2ScanCtrl = AntPlusBikeCadencePcc.requestAsyncScanController(this, 0, new AntPlusBikeSpdCadCommonPcc.IBikeSpdCadAsyncScanResultReceiver() {
            @Override
            public void onSearchStopped(RequestAccessResult requestAccessResult) {
            }
            @Override
            public void onSearchResult(AntPlusBikeSpdCadCommonPcc.BikeSpdCadAsyncScanResultDeviceInfo bikeSpdCadAsyncScanResultDeviceInfo) {
                String name = bikeSpdCadAsyncScanResultDeviceInfo.resultInfo.getDeviceDisplayName();
                if(name.startsWith(moxyStart)) {
                    if(moxy2 == null && !name.equals(moxy.getDeviceDisplayName())) {
                        setMoxy2(bikeSpdCadAsyncScanResultDeviceInfo.resultInfo);
                    }
                }
            }
        });
    }

    public void setMoxy(AsyncScanController.AsyncScanResultDeviceInfo data)
    {
        moxy = data;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                moxyText.append("\nFound MOXY");
            }
        });
        moxyReleaseHandle = moxyScanCtrl.requestDeviceAccess(data, new AntPluginPcc.IPluginAccessResultReceiver<AntPlusBikeCadencePcc>() {
            @Override
            public void onResultReceived(AntPlusBikeCadencePcc AntPlusBikeCadencePcc, RequestAccessResult requestAccessResult, DeviceState deviceState) {
                AntPlusBikeCadencePcc.subscribeCalculatedCadenceEvent(new AntPlusBikeCadencePcc.ICalculatedCadenceReceiver() {
                    @Override
                    public void onNewCalculatedCadence(long l, EnumSet<EventFlag> enumSet, BigDecimal bigDecimal) {
                        final BigDecimal mx = bigDecimal;
                        lastMoxy = bigDecimal.toString();
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                moxyText.setText("MX: " + mx);
//                            }
//                        });
                    }
                });

                if(AntPlusBikeCadencePcc.isSpeedAndCadenceCombinedSensor()) {
                    AntPlusBikeSpeedDistancePcc.requestAccess(MainActivity.this, AntPlusBikeCadencePcc.getAntDeviceNumber(), 0, true, new AntPluginPcc.IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc>() {
                        @Override
                        public void onResultReceived(AntPlusBikeSpeedDistancePcc antPlusBikeSpeedDistancePcc, RequestAccessResult requestAccessResult, DeviceState deviceState) {
                            antPlusBikeSpeedDistancePcc.subscribeCalculatedSpeedEvent(new AntPlusBikeSpeedDistancePcc.CalculatedSpeedReceiver(new BigDecimal(2.095)) {
                                @Override
                                public void onNewCalculatedSpeed(long l, EnumSet<EventFlag> enumSet, BigDecimal bigDecimal) {
                                    lastFlow = bigDecimal.toString();
                                }
                            });
                        }
                    }, moxy_IDeviceStateChangeReceiver);
                }
            }
        }, moxy_IDeviceStateChangeReceiver);
    }


    public void setMoxy2(AsyncScanController.AsyncScanResultDeviceInfo data)
    {
        moxy2 = data;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                moxyText.append("\nFound MOXY2");
            }
        });
        String foo = "ar";

        moxy2ReleaseHandle = moxy2ScanCtrl.requestDeviceAccess(data, new AntPluginPcc.IPluginAccessResultReceiver<AntPlusBikeCadencePcc>() {
            @Override
            public void onResultReceived(AntPlusBikeCadencePcc AntPlusBikeCadencePcc, RequestAccessResult requestAccessResult, DeviceState deviceState) {
                AntPlusBikeCadencePcc.subscribeCalculatedCadenceEvent(new AntPlusBikeCadencePcc.ICalculatedCadenceReceiver() {
                    @Override
                    public void onNewCalculatedCadence(long l, EnumSet<EventFlag> enumSet, BigDecimal bigDecimal) {
                        final BigDecimal mx = bigDecimal;
                        lastMoxy2 = bigDecimal.toString();
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                moxyText.setText("MX2: " + mx);
//                            }
//                        });
                    }
                });

                if(AntPlusBikeCadencePcc.isSpeedAndCadenceCombinedSensor()) {
                    AntPlusBikeSpeedDistancePcc.requestAccess(MainActivity.this, AntPlusBikeCadencePcc.getAntDeviceNumber(), 0, true, new AntPluginPcc.IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc>() {
                        @Override
                        public void onResultReceived(AntPlusBikeSpeedDistancePcc antPlusBikeSpeedDistancePcc, RequestAccessResult requestAccessResult, DeviceState deviceState) {
                            antPlusBikeSpeedDistancePcc.subscribeCalculatedSpeedEvent(new AntPlusBikeSpeedDistancePcc.CalculatedSpeedReceiver(new BigDecimal(2.095)) {
                                @Override
                                public void onNewCalculatedSpeed(long l, EnumSet<EventFlag> enumSet, BigDecimal bigDecimal) {
                                    lastFlow2 = bigDecimal.toString();
                                }
                            });
                        }
                    }, moxy_IDeviceStateChangeReceiver);
                }
            }
        }, moxy_IDeviceStateChangeReceiver);
    }

    protected AntPluginPcc.IDeviceStateChangeReceiver moxy_IDeviceStateChangeReceiver =
            new AntPluginPcc.IDeviceStateChangeReceiver()
            {
                @Override
                public void onDeviceStateChange(final DeviceState newDeviceState)
                {
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            moxyText.append("\nNew state: " + newDeviceState);
                        }
                    });


                }
            };
}
